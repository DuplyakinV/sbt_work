package version_2;

import static org.junit.Assert.*;

import org.junit.Assert;
import version_2.A;
import version_2.Generator;
/**
 * Created by vlad on 20.09.16.
 */
public class ATest {

    private A[] all = null;

    @org.junit.Before
    public void setUp() throws Exception {
        Generator<A> gen = new Generator<A>();
        all = gen.generateAllData(A.class);
    }

    @org.junit.After
    public void tearDown() throws Exception {
        all = null;
    }

    @org.junit.Test
    public void copy() throws Exception {
        for (A a : all) {
            A copyA = a.copy();
            Assert.assertEquals("Copy not idential",a, copyA);
            Assert.assertEquals("Hash copy not idential", a.hashCode(), copyA.hashCode());
        }
    }

    @org.junit.Test
    public void equals() throws Exception {

    }


}